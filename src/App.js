import React from "react";
import "./assets/scss/index.scss";
import "antd/dist/antd.css";

import { useDispatch } from "react-redux";
import { fetchedProducts } from "./redux/actions";
import Header from "./components/common/Header";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./components/pages/Home";
import Settings from "./components/pages/Settings";
import Product from "./components/products/Product";

function App() {
  const dispatch = useDispatch();
  dispatch(fetchedProducts());

  return (
    <Router>
      <Header />
      <div className="container">
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/settings" component={Settings} />
          <Route path="/products/product/:slug" component={Product} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
