import React from "react";
import { Link } from "react-router-dom";

const Header = (props) => {
  return (
    <header className="header">
      <div className="container-inner">
        <Link to="/" className="logo">
          SPRITZ
        </Link>
        <ul className="menu">
          <li>
            <Link to="/">Головна</Link>
          </li>
          <li>
            <Link to="/settings">Налаштування</Link>
          </li>
        </ul>
        <Link to="/cart" className="cart">
          Корзина
        </Link>
      </div>
    </header>
  );
};

export default Header;
