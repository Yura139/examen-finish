import React, { useState } from "react";
import { Input, Form, Button, Upload, message } from "antd";
import { connect } from "react-redux";
import { createProduct } from "../../redux/actions";

const AddProduct = (props) => {
  const { setVisibleAdd } = props;
  const [values, setValues] = useState({ title: "", desc: "", price: "", country: "", image: "" });

  const { title, desc, price, country, image } = values;

  const translit = (str) => {
    var ru = "А-а-Б-б-В-в-Ґ-ґ-Г-г-Д-д-Е-е-Ё-ё-Є-є-Ж-ж-З-з-И-и-І-і-Ї-ї-Й-й-К-к-Л-л-М-м-Н-н-О-о-П-п-Р-р-С-с-Т-т-У-у-Ф-ф-Х-х-Ц-ц-Ч-ч-Ш-ш-Щ-щ-Ъ-ъ-Ы-ы-Ь-ь-Э-э-Ю-ю-Я-я".split("-");
    var en = "A-a-B-b-V-v-G-g-G-g-D-d-E-e-E-e-E-e-ZH-zh-Z-z-I-i-I-i-I-i-J-j-K-k-L-l-M-m-N-n-O-o-P-p-R-r-S-s-T-t-U-u-F-f-H-h-TS-ts-CH-ch-SH-sh-SCH-sch-'-'-Y-y-'-'-E-e-YU-yu-YA-ya".split("-");
    var res = "";
    for (var i = 0, l = str.length; i < l; i++) {
      var s = str.charAt(i),
        n = ru.indexOf(s);
      if (n >= 0) {
        res += en[n];
      } else {
        res += s;
      }
    }
    return res.split(" ").join("_");
  };

  const changeInput = (name) => (e) => {
    let value;
    if (name === "image") {
      value = URL.createObjectURL(e.target.files[0]);
    } else {
      value = e.target.value;
    }
    setValues({ ...values, [name]: value });
  };

  const onSubmitForm = (e) => {
    if (title.length > 0 && desc.length > 0 && price.length > 0 && country.length > 0) {
      let slug = translit(title);

      const newProduct = {
        id: Date.now(),
        title: title,
        slug: slug,
        desc: desc,
        price: price,
        country: country,
      };

      props.createProduct(newProduct);
      setValues({ title: "", slug: "", desc: "", price: "", country: "", image: "" });
      setVisibleAdd(false);
    } else {
      message.error("Заповніть всі поля");
    }
  };

  return (
    <div className="add-product">
      <Form onFinish={onSubmitForm}>
        <Form.Item label="Назва: " name="title">
          <Input placeholder="Введіть назву" value={title} onChange={changeInput("title")} />
        </Form.Item>

        <Form.Item label="Країна: " name="country">
          <Input placeholder="Введіть країну" value={country} onChange={changeInput("country")} />
        </Form.Item>

        <Form.Item label="Ціна: " name="price">
          <Input placeholder="Введіть ціну" value={price} onChange={changeInput("price")} />
        </Form.Item>

        <Form.Item name="desc" label="Опис: ">
          <Input.TextArea placeholder="Напишіть опис до товару" value={desc} onChange={changeInput("desc")} />
        </Form.Item>

        <input type="file" src={image} onChange={changeInput("image")} />

        <Button type="primary" htmlType="submit">
          Добавити
        </Button>
      </Form>
    </div>
  );
};

const mapDispatchToProps = {
  createProduct: createProduct,
};

export default connect(null, mapDispatchToProps)(AddProduct);
