import React from "react";
import { useHistory } from "react-router-dom";

export default function ProductItem({ product }) {
  const history = useHistory();
  return (
    <div className="card">
      <div className="card-body">
        <div className="image" onClick={() => history.push(`/products/product/${product.slug}`)}>
          <img src={`/uploads/products/${product.image}`} alt={product.title} />
        </div>
        <h3> {product.title}</h3>
        <div className="info">
          <span className="price">{product.price} грн</span>
          <span className="country">{product.country}</span>
        </div>
        <div className="btn">Купити</div>
      </div>
    </div>
  );
}
