import React from "react";
import { connect } from "react-redux";
import ProductItem from "./ProductItem";

function ProductList({ data }) {
  if (!data.length) {
    return <p>empty</p>;
  }
  return (
    <div className="product-list">
      {data.map((product) => (
        <ProductItem product={product} key={product.id} />
      ))}
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    data: state.products.data,
  };
};

export default connect(mapStateToProps, null)(ProductList);
