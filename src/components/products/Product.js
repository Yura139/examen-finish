import React from "react";
import { connect } from "react-redux";

const Product = (props) => {
  const slug = props.match.params.slug;
  const data = props.data;
  const product = data && data.find((item) => item.slug === slug);
  console.log(product);
  return (
    <div className="container-inner">
      <div className="product-single">
        <div className="image">
          <img src={`/uploads/products/${product.image}`} alt="" />
        </div>
        <div className="desc">
          <h2>{product.title}</h2>
          <p>{product.desc}</p>
          <div className="info">
            <span>
              <b>Ціна: </b>
              {product.price}
            </span>
            <span>
              <b>Країна: </b>
              {product.country}
            </span>
          </div>
          <div className="btn">Купити</div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    data: state.products.data,
  };
};

export default connect(mapStateToProps, null)(Product);
