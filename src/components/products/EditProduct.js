import { Button, Form, message } from "antd";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { editProduct } from "../../redux/actions";

const EditProduct = ({ product, setVisibleEdit }) => {
  const [values, setValues] = useState({ title: product.title, desc: product.desc, price: product.price, country: product.country });

  const { title, desc, price, country } = values;

  const dispatch = useDispatch();

  const changeInput = (name) => (e) => {
    let value = e.target.value;
    setValues({ ...values, [name]: value });
  };

  const onSubmitForm = (e) => {
    if (title.length > 0 && desc.length > 0 && price.length > 0 && country.length > 0) {
      const newProduct = {
        id: product.id,
        title: title,
        slug: product.slug,
        desc: desc,
        price: price,
        country: country,
        image: product.image,
      };
      dispatch(editProduct(newProduct));
      setValues({ title: "", slug: "", desc: "", price: "", country: "", image: "" });
      setVisibleEdit(false);
    } else {
      message.error("Заповніть всі поля");
    }
  };

  return (
    <div className="edit-product">
      <Form onFinish={onSubmitForm}>
        <div className="form-item">
          <label htmlFor="title">Назва</label>
          <input id="title" value={title} onChange={changeInput("title")} />
        </div>
        <div className="form-item">
          <label htmlFor="country">Країна:</label>
          <input value={country} id="country" onChange={changeInput("country")} />
        </div>
        <div className="form-item">
          <label htmlFor="price">Ціна:</label>
          <input value={price} id="price" onChange={changeInput("price")} />
        </div>

        <div className="form-item">
          <label htmlFor="desc">Опис:</label>
          <textarea value={desc} id="desc" onChange={changeInput("desc")} />
        </div>

        <Button type="primary" htmlType="submit">
          Редагувати
        </Button>
      </Form>
    </div>
  );
};
export default EditProduct;
