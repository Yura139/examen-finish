import React from "react";
import ProductList from "../products/ProductList";

const Home = (props) => {
  return (
    <div className="container-inner">
      <h1>Home</h1>
      <ProductList />
    </div>
  );
};

export default Home;
