import { Drawer } from "antd";
import React, { useState } from "react";
import { connect, useDispatch } from "react-redux";
import AddProduct from "../products/AddProduct";
import { deleteProduct } from "../../redux/actions";
import EditProduct from "../products/EditProduct";

const Settings = ({ data }) => {
  const [visibleAdd, setVisibleAdd] = useState(false);
  const [visibleEdit, setVisibleEdit] = useState(false);
  const [productEdit, setProductEdit] = useState({});

  const dispatch = useDispatch();

  const showDrawerAdd = () => {
    setVisibleAdd(true);
  };
  const onCloseAdd = () => {
    setVisibleAdd(false);
  };

  const showDrawerEdit = (product) => {
    setProductEdit(product);
    setVisibleEdit(true);
  };

  const onCloseEdit = () => {
    setVisibleEdit(false);
  };

  return (
    <div className="container-inner settings">
      <h1>Settings</h1>
      <div className="btn add-product" onClick={showDrawerAdd}>
        Add products
      </div>
      <div className="list">
        {data &&
          data.map((item, key) => {
            return (
              <div className="item-list" key={key}>
                <span>{item.title}</span>
                <div className="btns">
                  <div className="btn" onClick={() => showDrawerEdit(item)}>
                    Edit
                  </div>
                  <div className="btn delete" onClick={() => dispatch(deleteProduct(item.id))}>
                    Delete
                  </div>
                </div>
              </div>
            );
          })}
      </div>

      <Drawer width={640} title="Добавити товар" placement="right" closable={false} onClose={onCloseAdd} visible={visibleAdd}>
        <AddProduct setVisibleAdd={setVisibleAdd} />
      </Drawer>

      <Drawer width={640} title="Редагувати товар" placement="right" closable={false} onClose={onCloseEdit} visible={visibleEdit}>
        <EditProduct setVisibleEdit={setVisibleEdit} product={productEdit} />
      </Drawer>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    data: state.products.data,
  };
};

export default connect(mapStateToProps, null)(Settings);
