import { CREATE_PRODUCT, DELETE_PRODUCT, FETCH_PRODUCTS, EDIT_PRODUCT } from "./types";
import products from "../data/products";

export function createProduct(product) {
  return {
    type: CREATE_PRODUCT,
    payload: product,
  };
}

export function fetchedProducts() {
  return async (dispatch) => {
    dispatch({
      type: FETCH_PRODUCTS,
      payload: products,
    });
  };
}

export function deleteProduct(id) {
  return {
    type: DELETE_PRODUCT,
    payload: id,
  };
}

export function editProduct(product) {
  return {
    type: EDIT_PRODUCT,
    payload: product,
  };
}
