import { CREATE_PRODUCT, DELETE_PRODUCT, FETCH_PRODUCTS, EDIT_PRODUCT } from "./types";

const inialState = {
  data: [],
  selected: null,
};

export const productsReducer = (state = inialState, action) => {
  switch (action.type) {
    case CREATE_PRODUCT:
      return { ...state, data: [...state.data, action.payload] };

    case FETCH_PRODUCTS:
      return { ...state, data: action.payload };

    case DELETE_PRODUCT:
      const currentId = action.payload;
      let products = state.data;
      products = products.filter((product) => product.id !== currentId);

      return { ...state, data: products };

    case EDIT_PRODUCT:
      const currentProduct = action.payload;
      let updateProducts = state.data;

      updateProducts[updateProducts.findIndex((el) => el.id === currentProduct.id)] = currentProduct;

      return { ...state, data: updateProducts };

    default:
      return state;
  }
};
